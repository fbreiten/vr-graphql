const fetch = require('node-fetch');
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList
  } = require('graphql')

const stationMap = new Map();

async function fetchStationList() {
    return await fetch(`https://rata.digitraffic.fi/api/v1/metadata/stations`)
        .then(response => response.json())
        .then(stations => stations.map(station => stationMap.set(station.stationShortCode, station.stationName)))
}

async function getStation(shortCode) {
    if (stationMap.size === 0) {
        await fetchStationList();
    }

    return stationMap.get(shortCode);
}

const StationType = new GraphQLObjectType({
    name: 'Station',
    description: '...',

    fields: () => ({
        shortCode: {
            type: GraphQLString,
            resolve: station =>
                station.stationShortCode
        },
        delayedInMin: {
            type: GraphQLInt,
            resolve: station =>
                station.differenceInMinutes
        },
        schedule: {
            type: GraphQLString,
            resolve: station =>
                station.scheduledTime
        },
        name: {
            type: GraphQLString,
            resolve: station => getStation(station.stationShortCode)
        }
    })
})

const TrainType = new GraphQLObjectType({
    name: 'Train',
    description: '...',

    fields: () => ({
        number: {
            type: GraphQLInt,
            resolve: json =>
                json[0].trainNumber
        },
        station: {
            type: new GraphQLList(StationType),
            resolve: json =>
                json[0].timeTableRows
        }
    })
})

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        description: '...',

        fields: () => ({
            train: {
                type: TrainType,
                args: {
                    id: { type: GraphQLInt }
                },
                resolve: (root, args, context) => context.fetchTrain(args.id)
            }
        })
    })
})