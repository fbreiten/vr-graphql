const express = require('express')
const graphqlHTTP = require('express-graphql')
const app = express();
const fetch = require('node-fetch');
const schema = require('./schema');


const fetchTrain = id =>
    fetch(`https://rata.digitraffic.fi/api/v1/trains/latest/${id} `)
        .then(response => response.json())

app.use('/graphql', graphqlHTTP(reg => {

    return {
        schema,
        context: {
            fetchTrain
        },
        graphiql: true
    }

}))
app.listen(4000);
console.log("Listening . . .")

// fetchTrain(1).then(res => JSON.stringify(console.log(res[0]), null, 2))